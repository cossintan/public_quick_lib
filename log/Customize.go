package log

import (
	"fmt"
	"os"

	"github.com/go-kratos/kratos/v2/log"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type CustomizeLogger struct {
	log  *zap.Logger
	Sync func() error
}

// Log Implementation of logger interface
func (l *CustomizeLogger) Log(level log.Level, keyvals ...interface{}) error {
	if len(keyvals) == 0 || len(keyvals)%2 != 0 {
		l.log.Warn(fmt.Sprint("Keyvalues must appear in pairs: ", keyvals))
		return nil
	}

	// Zap.Field is used when keyvals pairs appear
	var data []zap.Field
	for i := 0; i < len(keyvals); i += 2 {
		data = append(data, zap.Any(fmt.Sprint(keyvals[i]), fmt.Sprint(keyvals[i+1])))
	}
	switch level {
	case log.LevelDebug:
		l.log.Debug("", data...)
	case log.LevelInfo:
		l.log.Info("", data...)
	case log.LevelWarn:
		l.log.Warn("", data...)
	case log.LevelError:
		l.log.Error("", data...)
	}
	return nil
}

const PathSeparator = string(os.PathSeparator)

// zap package - go.uber.org/zap - pkg.go.dev https://pkg.go.dev/go.uber.org/zap
func NewCustomizeLogger(Filename string) *CustomizeLogger {
	// For some users, the presets offered by the NewProduction, NewDevelopment,
	// and NewExample constructors won't be appropriate. For most of those
	// users, the bundled Config struct offers the right balance of flexibility
	// and convenience. (For more complex needs, see the AdvancedConfiguration
	// example.)
	//
	// See the documentation for Config and zapcore.EncoderConfig for all the
	// available options.

	// First, define our level-handling logic.
	highPriority := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
		return lvl >= zapcore.ErrorLevel
	})
	lowPriority := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
		return lvl < zapcore.ErrorLevel
	})

	en := func() zapcore.EncoderConfig {
		return zapcore.EncoderConfig{
			TimeKey:        "ts",
			LevelKey:       "level",
			NameKey:        "logger",
			CallerKey:      "caller",
			FunctionKey:    zapcore.OmitKey,
			MessageKey:     "msg",
			StacktraceKey:  "stacktrace",
			LineEnding:     zapcore.DefaultLineEnding,
			EncodeLevel:    zapcore.CapitalLevelEncoder,
			EncodeTime:     zapcore.ISO8601TimeEncoder,
			EncodeDuration: zapcore.SecondsDurationEncoder,
			EncodeCaller:   zapcore.ShortCallerEncoder,
		}
	}
	// zap package - go.uber.org/zap - pkg.go.dev https://pkg.go.dev/go.uber.org/zap
	// The bundled Config struct only supports the most common configuration
	// options. More complex needs, like splitting logs between multiple files
	// or writing to non-file outputs, require use of the zapcore package.
	coreHighPriority := zapcore.NewCore(
		zapcore.NewConsoleEncoder(en()),
		zapcore.NewMultiWriteSyncer(
			zapcore.AddSync(os.Stdout),
			zapcore.AddSync(IoWriter4SplitFile("logs"+PathSeparator+"ERROR-"+Filename+".log")),
		),
		highPriority)
	coreLowPriority := zapcore.NewCore(
		zapcore.NewJSONEncoder(en()),
		zapcore.NewMultiWriteSyncer(
			zapcore.AddSync(os.Stdout),
			zapcore.AddSync(IoWriter4SplitFile("logs"+PathSeparator+"DEBUG-"+Filename+".log")),
		),
		lowPriority)
	opts := []zap.Option{
		zap.AddStacktrace(zapcore.WarnLevel),
	}
	core := zapcore.NewTee(
		coreHighPriority,
		coreLowPriority,
	)
	l := zap.New(core, opts...)
	return &CustomizeLogger{log: l, Sync: l.Sync}
}
