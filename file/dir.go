package main

import (
	"os"
	"strings"
)

func ExtractDir(isDIr bool, p string) string {
	if isDIr {
		return p
	}
	ps := "/"
	l := strings.Split(p, ps)
	n := len(l) - 1
	p = strings.Join(l[0:n], ps)
	return p
}

func CreateDirIfIsNotExist(isDIr bool, p string, perm os.FileMode) error {
	// isDIr 由调用者声明是从文件或者目录，来创建目录（由具体的业务场景判断，因为无法从字符串判断）。
	d := ExtractDir(isDIr, p)
	_, err := os.Open(p)
	if os.IsNotExist(err) {
		// 注意路径穿越检查
		// secguide/Go安全指南.md at main · Tencent/secguide · GitHub https://github.com/Tencent/secguide/blob/main/Go%E5%AE%89%E5%85%A8%E6%8C%87%E5%8D%97.md#121%E5%BF%85%E9%A1%BB-%E8%B7%AF%E5%BE%84%E7%A9%BF%E8%B6%8A%E6%A3%80%E6%9F%A5
		err := os.MkdirAll(d, os.ModeDir)
		return err
	}
	return nil
}

func NewFile(name string, data []byte, perm os.FileMode) error {
	err := os.WriteFile(name, data, perm)
	return err
}
