package main

import (
	"os"
	"testing"
)

func TestCreateDirIfIsNotExist(t *testing.T) {
	type args struct {
		isDIr bool
		p     string
		perm  os.FileMode
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{name: "", args: args{isDIr: true, p: "test2/atxt", perm: 0777}},
		{name: "", args: args{isDIr: true, p: "test3/t1/atxt", perm: 0777}},
		{name: "", args: args{isDIr: true, p: "../../testa/t1/atxt", perm: 0777}},
		{name: "", args: args{isDIr: true, p: "../../../testb/t1/atxt", perm: 0777}},
		{name: "", args: args{isDIr: false, p: "atest2/a/b.txt", perm: 0777}},
		{name: "", args: args{isDIr: false, p: "atest3/t1/a/b.txt", perm: 0777}},
		{name: "", args: args{isDIr: false, p: "../../atest/t1/a/b.txt", perm: 0777}},
		{name: "", args: args{isDIr: false, p: "../../../atest/t1/a/b.txt", perm: 0777}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := CreateDirIfIsNotExist(tt.args.isDIr, tt.args.p, tt.args.perm); (err != nil) != tt.wantErr {
				t.Errorf("CreateDirIfIsNotExist() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestNewFile(t *testing.T) {
	type args struct {
		name string
		data []byte
		perm os.FileMode
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{name: "", args: args{name: "atest2/a/b.txt", data: []byte("1"), perm: 0777}},
		{name: "", args: args{name: "atest3/t1/a/b.txt", data: []byte("1"), perm: 0777}},
		{name: "", args: args{name: "../../atest/t1/a/b.txt", data: []byte("1"), perm: 0777}},
		{name: "", args: args{name: "../../../atest/t1/a/b.txt", data: []byte("1"), perm: 0777}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := NewFile(tt.args.name, tt.args.data, tt.args.perm); (err != nil) != tt.wantErr {
				t.Errorf("NewFile() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
